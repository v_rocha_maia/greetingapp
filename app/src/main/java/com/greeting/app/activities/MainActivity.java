package com.greeting.app.activities;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.greeting.app.R;
import com.greeting.app.broadcasts.GreetingBroadcastReceiver;
import com.greeting.app.data.provider.AppPreferences;
import com.greeting.app.intents.GreetingService;
import com.greeting.app.utils.Utils;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static MainActivity sMainActivity;
    private int PERMISSIONS_CODE = 10;

    public static MainActivity getInstance(){
        return sMainActivity;
    }

    private EditText mEdtFriendName;
    private ProgressDialog mProgressDialog;
    private GreetingBroadcastReceiver mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getResources().getString(R.string.str_sending_greeting));
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);

        sMainActivity = this;

        verifyExternalStoragePermissions();

        AppPreferences preferences = AppPreferences.getInstance(this);

        getSupportActionBar().setTitle(getResources().getString(R.string.str_welcome) + " "
                +preferences.getUserName());

        Button btnSendGreeting = (Button) findViewById(R.id.btn_sendGreeting);
        mEdtFriendName = (EditText) findViewById(R.id.edt_friendName);

        if(btnSendGreeting != null)
            btnSendGreeting.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(!Utils.checkNetworkConnection(this)){

            SweetAlertDialog dialog = new SweetAlertDialog(this,SweetAlertDialog.ERROR_TYPE);
            dialog.setTitleText(getResources().getString(R.string.str_dialog_no_internet_title));
            dialog.setContentText(getResources().getString(R.string.str_dialog_no_internet_message));
            dialog.show();

        } else if(mEdtFriendName.getText().toString().trim().isEmpty())
            mEdtFriendName.setError(getResources().getString(R.string.str_error_mandatory_field));
          else {


            Intent intentService = new Intent(this,GreetingService.class);
            intentService.putExtra(GreetingService.FRIEND_NAME,mEdtFriendName.getText().toString());
            startService(intentService);

            showProgressDialog();

        }


    }


    private void verifyExternalStoragePermissions(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int permissionReadExternalStorage = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            int permissionWriteExternalStorage = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);

            String[] permissions = new String[2];
            boolean needToVerify = false;


            if(permissionReadExternalStorage != PackageManager.PERMISSION_GRANTED) {
                permissions[0] = Manifest.permission.READ_EXTERNAL_STORAGE;
                needToVerify = true;

            }

            if(permissionWriteExternalStorage != PackageManager.PERMISSION_GRANTED){
                if(permissions[0].isEmpty())
                    permissions[0] = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                else
                    permissions[1] = Manifest.permission.WRITE_EXTERNAL_STORAGE;

                needToVerify = true;
            }

            if(needToVerify)
                ActivityCompat.requestPermissions(this,permissions,PERMISSIONS_CODE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Here we can do something with the user response to the permissions requested.
    }

    private void showProgressDialog(){
        mProgressDialog.show();
    }
    public void hideProgressDialog(){

        mProgressDialog.hide();
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter(GreetingBroadcastReceiver.ACTION_RESP);
        mReceiver = new GreetingBroadcastReceiver();
        registerReceiver(mReceiver, intentFilter);

    }

    @Override
    protected void onStop() {
        super.onStop();
        try{unregisterReceiver(mReceiver);}
        catch (RuntimeException  e){ e.printStackTrace(); }
    }

}
