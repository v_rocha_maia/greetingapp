package com.greeting.app.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.greeting.app.R;
import com.greeting.app.data.provider.AppPreferences;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEdtUserName;
    private AppPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        mPreferences = AppPreferences.getInstance(this);

        if(!mPreferences.getUserName().isEmpty()) {
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        mEdtUserName = (EditText) findViewById(R.id.edt_userName);
        Button btnSave = (Button) findViewById(R.id.btn_saveName);

        if(btnSave != null)
            btnSave.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {

        if(mEdtUserName.getText().toString().trim().isEmpty()) {
            mEdtUserName.setError(getResources().getString(R.string.str_error_mandatory_field));

        } else {

            mPreferences.setUserName(mEdtUserName.getText().toString());

            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
