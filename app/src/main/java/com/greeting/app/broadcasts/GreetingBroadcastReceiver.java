package com.greeting.app.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;

import com.greeting.app.R;
import com.greeting.app.activities.MainActivity;
import com.greeting.app.intents.GreetingService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by vroch on 02/07/2016.
 */
public class GreetingBroadcastReceiver extends BroadcastReceiver {

    public static final String ACTION_RESP =
            "com.greeting.app.broadcast.receiver";

    @Override
    public void onReceive(final Context context, Intent intent) {

        final MainActivity mainActivity = MainActivity.getInstance();

        if(mainActivity != null)
            mainActivity.hideProgressDialog();

        final String message = intent.getStringExtra(GreetingService.GREETING_CONTENT);

        if(message != null){
            final SweetAlertDialog dialog = new SweetAlertDialog(context,SweetAlertDialog.SUCCESS_TYPE);


            dialog.setTitleText(message);
            dialog.setConfirmText(context.getResources().getString(R.string.dlg_btn_save));
            dialog.setCancelText(context.getResources().getString(R.string.dlg_btn_close));
            dialog.showCancelButton(true);
            dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    SaveGreetingMessageTask task = new SaveGreetingMessageTask();
                    task.execute(message);
                    dialog.dismiss();

                }
            });
            dialog.show();

        } else{

            SweetAlertDialog dialog = new SweetAlertDialog(context,SweetAlertDialog.ERROR_TYPE);
            dialog.setTitleText(context.getString(R.string.str_dialog_server_error_title));
            dialog.setContentText(context.getString(R.string.str_dialog_server_error_message));
            dialog.show();
        }


    }

    private class SaveGreetingMessageTask extends AsyncTask<String,Void,Boolean>{


        @Override
        protected Boolean doInBackground(String... params) {

            File file;
            FileOutputStream fileOutputStream;
            OutputStreamWriter outWriter;

            try {

                file = new File(Environment.getExternalStorageDirectory().getPath()+"/greetingMessages.txt");

                if(!file.exists())
                    file.createNewFile();

                fileOutputStream =  new FileOutputStream(file,true);
                outWriter = new OutputStreamWriter(fileOutputStream);
                outWriter.append(params[0]);
                outWriter.append(" / ");
                outWriter.append(new Date()+"");
                outWriter.append("\r\n");
                outWriter.close();
                fileOutputStream.close();

                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return  false;
        }

        @Override
        protected void onPostExecute(Boolean response) {
            // Do something according to the response variable
        }
    }


}
