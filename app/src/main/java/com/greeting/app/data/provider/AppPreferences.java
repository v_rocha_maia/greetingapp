package com.greeting.app.data.provider;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by vroch on 02/07/2016.
 */
public class AppPreferences {

    private static final String PREFERENCES_FILE = "PreferencesFile";

    private static final String USER_NAME = "user_name";

    private static AppPreferences sInstance;
    private SharedPreferences mSharedPreferences;

    public static synchronized AppPreferences getInstance(Context context){

        if(sInstance == null)
            sInstance = new AppPreferences(context);

        return sInstance;
    }

    public AppPreferences(Context context){
        mSharedPreferences = context.getSharedPreferences(PREFERENCES_FILE,Context.MODE_PRIVATE);
    }

    public void setUserName(String userName){
        mSharedPreferences.edit().putString(USER_NAME,userName).apply();
    }

    public String getUserName() {
        return mSharedPreferences.getString(USER_NAME,"");
    }
}
