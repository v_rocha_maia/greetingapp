package com.greeting.app.data.model;

/**
 * Created by vroch on 02/07/2016.
 */
public class Greeting {

    private String id;
    private String content;

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
