package com.greeting.app.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by vroch on 02/07/2016.
 */
public class Utils {

    public static boolean checkNetworkConnection(Context context) {
        boolean status = false;

        try {
            ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);

            if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
                status = true;
            }
            else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
                    status = true;
            }

        } catch (Exception e){
            e.printStackTrace();
            return false;
        }



        return status;
    }


}
