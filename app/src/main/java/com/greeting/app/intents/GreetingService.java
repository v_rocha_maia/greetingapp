package com.greeting.app.intents;

import android.app.IntentService;
import android.content.Intent;

import com.greeting.app.BuildConfig;
import com.greeting.app.broadcasts.GreetingBroadcastReceiver;
import com.greeting.app.data.model.Greeting;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 * Created by vroch on 02/07/2016.
 */
public class GreetingService extends IntentService {

    public static final String FRIEND_NAME = "friend_name";
    public static final String GREETING_CONTENT = "greeting_content";
    private static final String PREMIUM = "premium";
    private static final String BASIC = "basic";


    public GreetingService(){
        super("GreetingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String friendName = intent.getStringExtra(FRIEND_NAME);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        Greeting greeting = null;

        try{
            if(BuildConfig.FLAVOR.equals(PREMIUM))
                greeting = restTemplate.getForObject(BuildConfig.SERVICE_URL+friendName,Greeting.class);
            else if(BuildConfig.FLAVOR.equals(BASIC))
                greeting = restTemplate.getForObject(BuildConfig.SERVICE_URL,Greeting.class);

        } catch (Exception e){
            e.printStackTrace();
        }

        Intent broadcastIntent = new Intent(GreetingBroadcastReceiver.ACTION_RESP);
        broadcastIntent.putExtra(GREETING_CONTENT,greeting != null ? greeting.getContent() : null);
        sendBroadcast(broadcastIntent);


    }



}
