GreetingApp

This app let you send “Hello” greetings to friends through a Spring RESTful Api, when it has been done successfully you are going to receive the response and then
the app will show an alert message with the content response, the user could choose if save this greeting
response in a file inside his phone or not. The user can be able to send unlimited greetings messages.

Besides that, the application have two types of builds, the premium and the basic one. In the premium version you can send messages with a custom friend name. In the basic version you just can send and receive a default message from the server. For select the version which you want, in Android Studio click on the "Build" menu option and in the "Select build variant" sub-option.